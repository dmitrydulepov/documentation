## Description

CHANGEME

## Workflow

### Preparation

* [ ] create feature branch <ticket-id>-<titel> (1-my-fix)
* [ ] assign the merge request to yourself
* [ ] verify merge request on https://test.docs.opsone.ch/1-my-fix
* [ ] if you not sure or you do a bigger change ask a co-worker for review/feedback
* [ ] merge the merge request
* [ ] verify it on the live site [https://docs.opsone.ch](https://docs.opsone.ch)
* [ ] close ticket

